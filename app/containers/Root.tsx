import React from 'react';
import { hot } from 'react-hot-loader/root';
import App from './App';
import HomePage from './HomePage';

const Root = () => (
  <App>
  <HomePage/>
</App>
);

export default hot(Root);
