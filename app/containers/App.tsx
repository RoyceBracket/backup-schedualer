import React, { ReactNode } from 'react';
import jss from 'jss';
import preset from 'jss-preset-default';

jss.setup(preset());

type Props = {
  children: ReactNode;
};

export default function App(props: Props) {
  const { children } = props;
  return <>{children}</>;
}
