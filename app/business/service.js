import cp from 'child_process';
import { v1 as uuid } from 'uuid';
import fs from 'fs';
import path from 'path';

const exec = (command, options) => new Promise((resolve, reject) => cp.exec(command, options, (error, stdout, stderr) => {
    if (error) reject(error.message);
    if (stderr) console.log(`stderr: ${stderr}`);
    resolve(stdout.toString());
}))

export default class {
    constructor({tasksDir = path.join(process.cwd(), `tasks`)}={}){
        this.tasksDir = tasksDir;
        if (!fs.existsSync(tasksDir)) fs.mkdirSync(tasksDir);
    }

    getTasks() {
        return exec('schtasks /query /fo LIST /v')
            .then(d => d.split('\n')
                .filter(s => s && s.toLowerCase().includes('taskname'))
                .map((s) => s.split(':').pop().trim())
                .filter(s => s && s.startsWith('\\backupper-')))
    }

    async addTask({ dir, repo, time }) {
        const {tasksDir} = this;
        const fileName = `${uuid()}.sh`;

        if (!fs.existsSync(path.join(dir, '.git'))) exec(
            ['git init && git add * && git commit --allow-empty -m "backuper"',
                `git remote add origin ${repo}`,
                'git push -u origin master'].join(' && ')
            , { cwd: dir });


        const fileContent = await fs.readFileSync(path.join(process.cwd(), 'app', 'business', 'file-template'));
        fs.writeFileSync(path.join(tasksDir, fileName), fileContent.toString().replace('{{DIR_NAME}}', dir))
        exec(`SchTasks /Create /SC DAILY /TN backupper-${fileName.split('-').pop()} /TR "${path.join(tasksDir, fileName)}" /ST ${time}`)
    }
}