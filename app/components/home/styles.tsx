import {createUseStyles} from 'react-jss'

export default createUseStyles({
    container:{
        
    },
    form:{
        '& input':{
            display:'block'
        }
    }
})