import React from 'react';

type InputProps = {
  name: string;
  value: string;
  onChange: (e: string) => void;
  placeholder?: string;
  type?: string;
};

const Input = ({
  value,
  name,
  placeholder = name.replace('_', ' '),
  onChange,
  type = 'text'
}: InputProps) => (
  <div style={{ margin: 5 }}>
    <label htmlFor={name}>{placeholder}</label>
    <input
      value={value}
      name={name}
      type={type}
      onChange={e => onChange(e.target.value.toString())}
    />
  </div>
);

export default Input;
