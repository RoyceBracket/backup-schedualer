/* eslint-disable no-console */
import React, { useState } from 'react';
import useStyles from './styles';
import BL from '../../business';
import Input from './input';

export default function Home() {
  const styles = useStyles();

  const [dir, setDir] = useState(
    `C:/Users/Assaf/AppData/Roaming/DarkSoulsIII/01100001153d95ff`
  );
  const [repo, setRepo] = useState(
    `https://RoyceBracket@bitbucket.org/RoyceBracket/test-repo.git`
  );
  const [time, setTime] = useState('02:00');

  BL.getTasks()
    .then(t => console.log('current tasks', t))
    .catch(console.error);
  return (
    <div className={styles.container}>
      <div className={styles.form}>
        <Input value={dir} onChange={setDir} name="directory name" />
        <Input value={repo} onChange={setRepo} name="git reposetory" />
        <Input value={time} onChange={setTime} name="time" />
        <button type="submit" onClick={() => BL.addTask({ dir, repo, time })}>
          submit
        </button>
      </div>
    </div>
  );
}
