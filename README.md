# Backuper

## Requirements

- runs on windows ONLY
- git (you can download it [here](https://git-scm.com/download/win))

## Instructions

### What is this

its a UI that backups selected directory to git on scheduled time

### How do i use it

after running `yarn package` in the project dir, an executable will be available in `/release`.

### How it does it

it takes three input: dir to backup, git repo, and time to run the process (daily). once submited, two things occur:

1. a new file is generated in tasks folder (tasks folder will be created next to exec if not exist)
1. a new task is created in [windows schedualer](https://docs.microsoft.com/en-us/windows/win32/taskschd/task-scheduler-start-page) to push that dir to the repo

to be honest, i use it to backup games saves

## Development

### Important dirs\files

in app dir:

- `components\home` - application UI
- `business` - file & task generator

the rest was bootstrapped from [electron-react-boilerplate](https://github.com/electron-react-boilerplate/electron-react-boilerplate)

### How to run it

in you favorite teminal run the following:

- `yarn` - install project dependencies
- `yarn dev` - starts dev mode
- `yarn package` - pack the project to various executibles in release dir
